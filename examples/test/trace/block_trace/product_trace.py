def test_throughput():
    pace=0.1
    with open("throughput.txt",'w') as f:
        t=0.0
        while t<=20:
            t+=0.01
            for _ in range(2):
                f.write("{:.2f}    50    1000000    1\n".format(0.01))
                f.write("{:.2f}    50    1000000    1\n".format(0.0)) #2*1MB/10ms=200MB/s=1.6Gbps

if __name__=='__main__':
    test_throughput()