#!/usr/bin/env python3

'''
Usage:
    pytest test.py
    pytest test.py -k base
    pytest test.py -k throughput
    ...
'''

import os
import time

class TestModule:
    def test_base(self):
        os.system("cd .. && make kill && make test")
    def test_throughput(self):
        os.system("cd .. && make kill && make client server library")
        os.system("cp ../client ./bin/client")
        os.system("cp ../server ./bin/server")
        trace="'trace/block_trace/throughput.txt'"
        os.system("LD_LIBRARY_PATH=./lib RUST_BACKTRACE=1 RUST_LOG=debug ./bin/server 127.0.0.1 5555 {} &> server_err.log &".format(trace))
        time.sleep(0.1)
        os.system("./run_client.sh")
    def test_clean(self):
        os.system("cd .. && make kill")
        os.system("rm -rf *.log")
        os.system("rm -rf ./bin/*")
        os.system("rm -rf ./lib/*")