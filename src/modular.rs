// provide 4 modes
pub enum Mode {
    PerEvent,
    NEvent,
    // Periodic,
    // Unperiodic,
}

pub struct Module {
    mode: Mode,
    n_to_call: u64,
    n: u64,
    // T: u64,
    // t: u64,
}

impl Module {
    pub fn new()->Module {
        Module {
            mode: Mode::NEvent,
            n_to_call: 1,
            n: 1,
        }
    }
    // check if the module should to be called immediately
    pub fn call_now(&mut self) -> bool {
        match &self.mode {
            Mode::PerEvent => true,
            Mode::NEvent => {
                if self.n == self.n_to_call {
                    self.n = 1;
                    true
                } else {
                    self.n += 1;
                    false
                }
            },
        }
    }

    // todo
    pub fn change_mode() {}
}
